// setup express app
var express = require('express');
var port = process.env.PORT || 3000;
var app = express();

// enable parsing of HTTP request body
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// import assets
app.use(express.static(__dirname + '/views'));
app.use(express.static(__dirname + '/public'))

// connect todo routes
var todoRoutes = require('./routes/todos');
app.use('/api/todos', todoRoutes);

// root route
app.get('/', function(req, res) {
  res.sendFile('index.html')
});

// run app
app.listen(port, function() {
  console.log('App is running on port ' + port);
});
