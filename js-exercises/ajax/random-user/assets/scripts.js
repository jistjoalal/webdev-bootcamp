
fullName = document.querySelector("#fullname");
username = document.querySelector("#username");
email = document.querySelector("#email");
city = document.querySelector("#city");
img = document.querySelector("#avatar");
btn = document.querySelector(".footer");
url = "https://randomuser.me/api/"

btn.addEventListener("click", function() {
  getNewUser()
});

function getNewUser() {
  fetch(url)
  .then(handleErrors)
  .then(parseJSON)
  .then(updateProfile)
  .catch(function() {
    console.log(error);
  })
}

function handleErrors(request) {
  if (!request.ok) {
    throw Error(request.status);
  }
  return request;
}

function parseJSON(request) {
  return request.json();
}

function updateProfile(data) {
  results = data.results[0];
  fullName.innerHTML = capitalizeName(results.name.first, results.name.last);
  username.innerHTML = results.login.username;
  email.innerHTML = results.email;
  city.innerHTML = results.location.city;
  img.src = results.picture.medium;
}

function capitalizeName(first, last) {
  return capitalize(first) + " " + capitalize(last);
}

function capitalize(word) {
  return word[0].toUpperCase() + word.slice(1);
}