var url = 'https://ron-swanson-quotes.herokuapp.com/v2/quotes';

$('#xhr').click(function() {
  var XHR = new XMLHttpRequest();
  XHR.onreadystatechange = function () {
    if (XHR.readyState == 4 && XHR.status == 200) {
      $('#quote').text(JSON.parse(XHR.responseText)[0]);
    }
  }
  XHR.open("GET", url);
  XHR.send();
})

$('#fetch').click(function() {
  fetch(url)
  .then(function(req) {
    return req.json();
  })
  .then(function(data) {
    $('#quote').text(data[0])
  })
  .catch(function() {
    console.log(error);
  })
})

$('#jquery').click(function() {
  $.getJSON(url)
  .done(function(data) {
    $("#quote").text(data[0]);
  })
})

$('#axios').click(function() {
  axios.get(url)
  .then(function(res) {
    $('#quote').text(res.data[0]);
  })
})