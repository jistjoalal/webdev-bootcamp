
// simple increment function that logs counter
var counter = 0;
function incCounter() {
  counter++;
  console.log("Counter:", counter);
}

// runs callback after ms, returns promise to the setTimeout call
function runLater(callback, ms) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      resolve(callback());
    }, ms);
  });
}

// run incCounter after 1000ms,
// then run incCounter after 2000ms,
// then run incCounter after 3000ms
runLater(incCounter, 1000).then(function() {
  return runLater(incCounter, 2000);
}).then(function() {
  return runLater(incCounter, 3000);
});

