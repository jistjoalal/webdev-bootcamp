
function findIndex(arr, callback) {
  for (let i = 0; i < arr.length; i++) {
    if (callback(arr[i], i, arr)) {
      return i;
    }
  }
  return -1;
}

var langs = ["Java", "C++", "Python", "Ruby"]
var r = findIndex(langs, function(lang, index, arr) {
  return lang === "Ruby";
});

console.log(r);

