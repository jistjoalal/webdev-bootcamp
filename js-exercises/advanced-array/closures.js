// closure exists when inner function makes use of variables
// defined in an outer function which has already returned.
// - used to isolate private variables and write "better code"
// - it seems like making a class for an object to me but whatever

// multiply 2 values, or return a function which accepts the 2nd value, if only 1 is passed
function specialMultiply(a,b) {
  if (arguments.length === 2) {
    return a * b;
  } else {
    return function mult(x) {
      return a * x;
    }
  }
}
console.log(specialMultiply(3,4));
console.log(specialMultiply(3)(4));
console.log(specialMultiply(3));


// start a guessing game w/ amount # of guesses
// calling game w/ a guess will return status
// on whether the guess was correct
// or the game is over
function guessingGame(amount) {
  var answer = Math.floor((Math.random() * 10) + 1);
  var guesses = 0;
  var done = false;
  return function game(guess) {
    if (done) {
      return 'You are all done playing!';
    }
    if (guesses >= amount) {
      done = true;
      return 'No more guesses the answer was ' + answer;
    }
    if (guess === answer) {
      done = true;
      return 'You got it!';
    } else if (guess > answer) {
      guesses++;
      return "You're too high!";
    } else {
      guesses++;
      return "You're too low!";
    }
  }
}
var game = guessingGame(5);
console.log(game(1));
console.log(game(2));
console.log(game(4));
console.log(game(3));
console.log(game(5));
console.log(game(6));

