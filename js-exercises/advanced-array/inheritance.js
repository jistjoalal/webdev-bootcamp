
function Vehicle(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
}
Vehicle.prototype.start = function() {
  return 'VROOM!';
}
Vehicle.prototype.toString = function() {
  return 'The make, model, and year are ' + [this.make, this.model, this.year].join(' ');
}
v = new Vehicle('toyota', 'camry', 1993);
console.log(v.start());
console.log(v.toString());

function Car() {
  // apply vehicle constructor
  Vehicle.apply(this, arguments);
  this.numWheels = 4;
}
// make car inherit methods from vehicle without overriding vehicles prototype
Car.prototype = Object.create(Vehicle.prototype);
// reset constructor
Car.prototype.constructor = Car;
c = new Car('toyota', 'othery', 1993);
console.log(c.start());
console.log(c.toString());
console.log(c.numWheels);

function Motorcycle() {
  Vehicle.apply(this, arguments);
  this.numWheels = 2;
}
Motorcycle.prototype = Object.create(Vehicle.prototype);
Motorcycle.prototype.constructor = Motorcycle;
m = new Motorcycle('honda', 'dirt', 1999);
console.log(m.numWheels);
console.log(m.start());

