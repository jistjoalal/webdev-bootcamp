

function Vehicle(make, model, year) {
  this.make = make;
  this.year = year;
  this.model = model;
  this.isRunning = false;
}

Vehicle.prototype.turnOn = function () {
  this.isRunning = true;
};

Vehicle.prototype.turnOff = function () {
  this.isRunning = false;
};

Vehicle.prototype.honk = function() {
  if (this.isRunning) {
    console.log('beep');
  }
};

v = new Vehicle('asdf', 'asdf', 1992);
console.log(v.isRunning);
v.turnOn();
console.log(v.isRunning);
v.honk();

function Person(firstName, lastName, favoriteColor, favoriteNumber) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.favoriteColor = favoriteColor;
    this.favoriteNumber = favoriteNumber;
    this.family = [];
}
Person.prototype.fullName = function() {
    return this.firstName + ' ' + this.lastName;
}
p = new Person('sheen', 'bob', 'green', 3);
console.log(p.fullName());

Person.prototype.addToFamily = function(p) {
  if (!(p in this.family)) {
    if (p instanceof Person) {
      this.family.push(p);
    }
  }
  return this.family.length;
}
console.log(p.family);
p2 = new Person('magic', 'cat', 'black', 1);
p.addToFamily(p2);
console.log(p.family);


// add map method to Array prototype
Array.prototype.map = function(callback) {
  let newArr = [];
  for (let i = 0; i < this.length; i++) {
    newArr.push(callback(this[i], i, this));
  }
  return newArr;
}
console.log([1,2,3].map(function(val) {
  return val * 2;
}));

// add reverse method to String prototype
String.prototype.reverse = function() {
  let newArr = [];
  for (let i = this.length-1; i >= 0; i--) {
    newArr.push(this[i]);
  }
  return newArr.join('');
}
console.log('asdfasdf'.reverse());

