
function map(arr, callback) {
  var arr2 = [];
  for (let i = 0; i < arr.length; i++) {
    arr2.push(callback(arr[i], i, arr));
  }
  return arr2;
}

let a = [1,2,3];
let b = map(a, function(val) {
  return val * 2;
});
console.log(b);


// returns new array w/ double values
function doubleValues(arr) {
  return map(arr, function(val) {
    return val * 2;
  });
}
console.log(doubleValues([1,2,3]));


// returns an array w/ each value mult by its index
function valTimesIndex(arr) {
  return map(arr, function(val, index) {
    return val * index;
  });
}
console.log(valTimesIndex([1,2,3]));


// returns value of key in each object of array
function extractKey(arr, key) {
  return map(arr, function(val) {
    return val[key];
  });
}
console.log(extractKey([{name: 'Elie'}, {name: 'Tim'}, {name: 'Matt'}], 'name'));


// combines first and last name object array into fullname string array
function extractFullName(arr) {
  return map(arr, function(val) {
    return val['first'] + ' ' + val['last'];
  });
}
console.log(extractFullName([{first: 'Elie', last: 'Schoppik'}, {first: 'Tim', last: 'Garcia'}]));


