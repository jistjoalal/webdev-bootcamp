
function filter(arr, callback) {
  var arr2 = [];
  for (var i = 0; i < arr.length; i++) {
    if (callback(arr[i], i, arr)) {
      arr2.push(arr[i]);
    }
  }
  return arr2;
}
a = ['asdf', 'qwer', 'zxcv', 'fff'];
console.log(filter(a, function(val) {
  return val.length > 3;
}));


// return array of objects w/ only objects that contain key
function filterByValue(arr, key) {
  return filter(arr, function(val) {
    return key in val;
  });
}
a = [{name: 'Elie'}, {name: 'Tim', isCatOwner: true}, {name: 'Matt'}];
console.log(filterByValue(a, 'isCatOwner'));


// returns first matching value from array
function find(arr, searchValue) {
  return filter(arr, function(val) {
    return val == searchValue;
  })[0];
}
console.log(find([1,2,3,4,5], 3));

// returns first object from array that matches search value for key
function findInObj(arr, key, searchValue) {
  return filter(arr, function(val) {
    return val[key] == searchValue;
  })[0];
}
a = [{name: 'Elie'}, {name: 'Tim', isCatOwner: true}, {name: 'Matt', isCatOwner: true}];
console.log(findInObj(a, 'isCatOwner', true));


// returns a lowercased string w/ all vowels removed
function removeVowels(str) {
  var arr = str.toLowerCase().split('');
  var vowels = ['a', 'e', 'i', 'o', 'u'];
  return filter(arr, function(val) {
    return vowels.indexOf(val) === -1;
  }).join('');
}
console.log(removeVowels('Elie'));


function map(arr, callback) {
  var arr2 = [];
  for (let i = 0; i < arr.length; i++) {
    arr2.push(callback(arr[i], i, arr));
  }
  return arr2;
}
// returns an array w/ odd numbers doubled
function doubleOddNumbers(arr) {
  var odds = filter(arr, function(val) {
    return val % 2 === 1;
  });
  return map(odds, function(val) {
    return val * 2;
  });
}
console.log(doubleOddNumbers([1,2,3,4,5,6]));

