
function some(arr, callback) {
  for (let i = 0; i < arr.length; i++) {
    if (callback(arr[i], i, arr)) {
      return true;
    }
  }
  return false;
}
console.log(some([1,2,3], function(val) {
  return val < 2;
}));

function every(arr, callback) {
  for (let i = 0; i < arr.length; i++) {
    if (!callback(arr[i], i, arr)) {
      return false;
    }
  }
  return true;
}
console.log(every([2,4,6,8], function(val) {
  return val % 2 === 0;
}));


// returns true if any number is odd
function hasOddNumber(arr) {
  return some(arr, function(val) {
    return val % 2 === 1;
  });
}
console.log(hasOddNumber([1,2,4,6,8]));


// returns true if number contains a digit
function hasAZero(num) {
  arr = (""+num).split("");
  return some(arr, function(val) {
    return val === '0';
  });
}
console.log(hasAZero(1237856870532478));


// returns true if array is only odd numbers
function hasOnlyOddNumbers(arr) {
  return every(arr, function(val) {
    return val % 2 === 1;
  });
}
console.log(hasOnlyOddNumbers([1,3,5,7,9]));


// returns true if no duplicates are in the array
function hasNoDuplicates(arr) {
  var clone = [];
  return every(arr, function(val) {
    var dup = val in clone;
    clone.push(val);
    return !dup;
  });
}
console.log(hasNoDuplicates([1,2,3,4]));


// returns true if every object in arr has key
function hasCertainKey(arr, key) {
  return every(arr, function(val) {
    return key in val;
  });
}
var arr = [{name: 'Elie'}, {name: 'Tim', isCatOwner: true}];
console.log(hasCertainKey(arr, 'name'));


// returns true if every object in arr has key matching searchValue
function hasCertainValue(arr, key, searchValue) {
  return every(arr, function(val) {
    return val[key] === searchValue;
  });
}
var arr = [{name: 'Elie', isCatOwner: true}, {name: 'Tim', isCatOwner: true}];
console.log(hasCertainValue(arr, 'isCatOwner', true));
