
function Dog(name, age) {
  this.name = name;
  this.age = age;
  this.bark = function() {
    console.log(this.name + ' just barked!');
  }
}

var rusty = new Dog('Rusty', 3);
rusty.bark();

function Vehicle(make, year, model) {
  this.make = make;
  this.year = year;
  this.model = model;
}
function Car() {
  Vehicle.apply(this, arguments);
  this.numWheels = 4;
}
function Motorcycle() {
  Vehicle.apply(this, arguments);
  this.numWheels = 2;
}
m = new Motorcycle('suzuki', 1993, 'fastmobile');
console.log(m.numWheels);
console.log(m.make);
console.log(m);


// PART 1

// Create a constructor function for a Person, each person should have a firstName, lastName, favoriteColor and favoriteNumber. Your function MUST be named Person.
function Person(firstName, lastName, favoriteColor, favoriteNumber) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.favoriteColor = favoriteColor;
    this.favoriteNumber = favoriteNumber;
    this.multiplyFavoriteNumber = function(num) {
        return num * this.favoriteNumber;
    }
}
// Write a method called multiplyFavoriteNumber that takes in a number and returns the product of the number and the object created from the Person functions' favorite number.


// PART 2

// Given the following code - refactor the Child function to remove all the duplication from the Parent function. You should be able to remove 4 lines of code in the Child function and replace it with 1 single line.

function Parent(firstName, lastName, favoriteColor, favoriteFood){
    this.firstName = firstName;
    this.lastName = lastName;
    this.favoriteColor = favoriteColor;
    this.favoriteFood = favoriteFood;
}

function Child(firstName, lastName, favoriteColor, favoriteFood){
    Parent.apply(this, arguments);
}

