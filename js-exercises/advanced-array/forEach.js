
// my implementation of forEach
function forEach(arr, callback) {
  for (let i = 0; i < arr.length; i++) {
    callback(arr[i], i, arr);
  }
}
a = [1,2,3];
forEach(a, function(val, index, arr) {
  console.log(val, index, arr);
});


// returns an array w/ all values doubled
function doubleValues(arr) {
  var arr2 = [];
  forEach(arr, function(val) {
    arr2.push(val * 2);
  });
  return arr2;
}
console.log(doubleValues(a));


// returns an array w/ odd values filtered
function onlyEvenValues(arr){
    var arr2 = [];
    forEach(arr, function(val) {
        if (val % 2 === 0) {
            arr2.push(val);
        }
    });
    return arr2;
}
console.log(onlyEvenValues(a));


// returns an array w/ only the first and last character of each element
function showFirstAndLast(arr){
    var arr2 = [];
    forEach(arr, function(val) {
        arr2.push(val[0] + val.slice(val.length-1));
    });
    return arr2;
}
console.log(showFirstAndLast(["asdf", "colt", "matt", "udeemy"]));


// adds a key value pair to an array of objects
function addKeyAndValue(arr,key,value){
    var arr2 = [];
    forEach(arr, function(val) {
        val[key] = value;
        arr2.push(val);
    });
    return arr2;
}
console.log(addKeyAndValue([{name: 'Elie'}, {name: 'Tim'}], 'title', 'instructor'));


// returns an object with keys=vowels and values=frequency in input string
function vowelCount(str){
   var vowels = ['a', 'e', 'i', 'o', 'u'];
   var counts = {};
   forEach(str, function(val) {
       var l = val.toLowerCase();
       if (vowels.includes(l)) {
           if (!(l in counts)) {
               counts[l] = 0;
           }
           counts[l] += 1;
       }
   });
   return counts;
}
console.log(vowelCount('Elie'));


